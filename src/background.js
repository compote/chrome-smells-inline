(function() {
    "use strict";

    var Dispatcher = function() {

    };

    Dispatcher.prototype.handler = function (request, sender, callback) {
        var actionHandler = 'action' + request.action;
        if (!this[actionHandler] || typeof this[actionHandler] !== 'function') {
            console.error('Unknown handler:' + request.action);
            return;
        }

        this[actionHandler].call(this, request.data, sender, callback);
    };

    Dispatcher.prototype.actionAddEvents = function(data, sender) {
        var formattedEvents = this.formatEvents(data);
        chrome.pageAction.setPopup({
            tabId: sender.tab.id,
            popup: 'popup.html?smells=' + encodeURIComponent(formattedEvents.join('<br>')) + '&tabId=' + sender.tab.id
        });
        this.setPageActionIcon(sender.tab.id, data.length);
        chrome.pageAction.show(sender.tab.id);
    };

    Dispatcher.prototype.actionGetAll = function(data, sender, callback) {
        chrome.tabs.sendMessage(
            data.tabId,
            { 'action': 'getAll' },
            callback
        );
    };

    Dispatcher.prototype.formatEvents = function(events) {
        var result = [];
        for (var i = 0, count = events.length; i < count; i++) {
            result.push(
                '<span' +
                    ' class="event-info"' +
                    ' data-element-id="' + events[i].elementId + '"' +
                    ' data-event-name="' + events[i].eventName + '"' +
                    ' role="element-shower"' +
                    '>' +
                    events[i].elementInfo + ' (' + events[i].eventName + ', length: ' + events[i].eventLength + ')' +
                    '</span>'
            );
        }

        return result;
    };

    Dispatcher.prototype.setPageActionIcon = function(tabId, count) {
        var canvas = document.createElement('canvas');
        var img = document.createElement('img');
        img.onload = function () {
            var context = canvas.getContext('2d');
            context.drawImage(img, 0, 2);
            if (count > 0) {
                context.fillStyle = "rgba(133,133,133,1)";
                context.fillRect(0, 10, 14, 19);
                context.fillStyle = 'black';
                context.font = '10px Arial';
                context.fillText(count, 1, 18);
            }

            chrome.pageAction.setIcon({
                imageData: context.getImageData(0, 0, 19, 19),
                tabId: tabId
            });
        };
        img.src = "icons/16.png";
    };

    var dispatcher = new Dispatcher();

    chrome.extension.onRequest.addListener(dispatcher.handler.bind(dispatcher));
})();