var Smell = {};
Smell.client = (function (window) {
"use strict";

    var Client = function () {
        this.events = {
            'onafterprint': true,
            'onbeforeprint': true,
            'onbeforeunload': true,
            'onerror': true,
            'onhaschange': true,
            'onload': true,
            'onmessage': true,
            'onoffline': true,
            'ononline': true,
            'onpagehide': true,
            'onpageshow': true,
            'onpopstate': true,
            'onredo': true,
            'onresize': true,
            'onstorage': true,
            'onundo': true,
            'onunload': true,
            'onblur': true,
            'onchange': true,
            'oncontextmenu': true,
            'onfocus': true,
            'onformchange': true,
            'onforminput': true,
            'oninput': true,
            'oninvalid': true,
            'onselect': true,
            'onsubmit': true,
            'onkeydown': true,
            'onkeypress': true,
            'onkeyup': true,
            'onclick': true,
            'ondblclick': true,
            'ondrag': true,
            'ondragend': true,
            'ondragenter': true,
            'ondragleave': true,
            'ondragover': true,
            'ondragstart': true,
            'ondrop': true,
            'onmousedown': true,
            'onmousemove': true,
            'onmouseout': true,
            'onmouseover': true,
            'onmouseup': true,
            'onmousewheel': true,
            'onscroll': true,
            'oncanplay': true,
            'oncanplaythrough': true,
            'ondurationchange': true,
            'onemptied': true,
            'onended': true,
            'onloadeddata': true,
            'onloadedmetadata': true,
            'onloadstart': true,
            'onpause': true,
            'onplay': true,
            'onplaying': true,
            'onprogress': true,
            'onratechange': true,
            'onreadystatechange': true,
            'onseeked': true,
            'onseeking': true,
            'onstalled': true,
            'onsuspend': true,
            'ontimeupdate': true,
            'onvolumechange': true,
            'onwaiting': true
        };

        this.elementsSelector = '[' + Object.keys(this.events).join('], [') + ']';
        this.observer = null;
    };

    Client.prototype.init = function() {
        this.checkNodeList(window.document.querySelectorAll(this.elementsSelector));
        this.checkInlineScripts();
        this.observer = new MutationSummary({
            callback: this.mutationHandler.bind(this),
            rootNode: window.document,
            queries: [
                {element: this.elementsSelector}
            ]
        });
    };

    Client.prototype.mutationHandler = function(changes) {
        this.checkNodeList(changes[0].added);
    };

    Client.prototype.checkNodeList = function(nodeList) {
        [].forEach.call(nodeList, this.checkElement, this);
    };

    Client.prototype.checkElement = function(element) {
        var events = [].filter.call(element.attributes, this.isInterestingEvent, this);

        var id = null;
        for (var i = 0, count = events.length; i < count; i++){
            if (events[i].value.length > 500) {
                if (id === null) {
                    id = 'smell_' + Math.random();
                    element.setAttribute('data-smells-id', id);
                }

                this.sendEvent({
                    'elementId': id,
                    'elementInfo': this.getElementInfo(element),
                    'eventName': events[i].name,
                    'eventLength': events[i].value.length
                });
            }
        }
    };

    Client.prototype.getElementInfo = function(element) {
        var result = element.tagName;
        if (!!element.id) {
            result += '#';
            result += element.id;
        } else if (!!element.className) {
            result += '.';
            result += element.className;
        }
        return result;
    };

    Client.prototype.isInterestingEvent = function(attribute) {
        return this.events.hasOwnProperty(attribute.name);
    };

    Client.prototype.checkInlineScripts = function()
    {
        var scripts = document.body.getElementsByTagName("script");
        var id = null;
        for (var i = 0; i < scripts.length; i++) {
            var script = scripts[i];
            if (script.innerHTML.length > 1000) {
                id = 'smell_' + Math.random();
                script.setAttribute('data-smells-id', id);
                this.sendEvent({
                    'elementId': id,
                    'elementInfo': this.getElementInfo(script),
                    'eventName': "script",
                    'eventLength': script.innerHTML.length
                });
            }
        }
    };

    Client.prototype.sendEvent = function(event) {
        window.document.dispatchEvent(
            new CustomEvent('EventToExtension', {
                'detail': event
            })
        );
    };

    var client = new Client();
    window.addEventListener('DOMContentLoaded' , client.init.bind(client), false);
    return client;
})(window);