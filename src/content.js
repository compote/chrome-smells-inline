(function() {
    "use strict";

    function attachScript(src) {
        var script = document.createElement('script');
        script.src = src;
        (document.head || document.documentElement).appendChild(script);
        script.onload = function () {
            script.parentNode.removeChild(script);
        };
    }

    attachScript(chrome.extension.getURL('lib/mutation-summary.js'));
    attachScript(chrome.extension.getURL('client.js'));

    var events = [];
    var timer = null;
    document.addEventListener('EventToExtension', function(event) {
        if (!event.detail)
            return;

        events.push(event.detail);
        if (!timer) {
            timer = window.setTimeout(function() {
                timer = null;
                chrome.extension.sendRequest({
                    'action': 'AddEvents',
                    'data': events
                });
            }, 2);
        }
    });

    chrome.runtime.onMessage.addListener(function(request, sender, callback) {
        switch(request.action) {
            case 'getAll':
                callback({'data': events});
                break;
            case 'clear':
                events = [];
                break;
            case 'show':
                var element = document.querySelector('[data-smells-id="' + request.data.id + '"]');
                console.log('Smells inline JS found:', {
                   'element': element,
                   'event': {
                       'name': request.data.eventName,
                       'text': element.getAttribute(request.data.eventName)
                   }
                });
                break;
            case 'showAll':
                console.log('Smells inline JS found:', events.map(function (item) {
                    var element = document.querySelector('[data-smells-id="' + item.elementId + '"]');
                    var text = 'unknown';
                    if (!element) {
                        element = item.elementInfo;
                    } else {
                        text = element.getAttribute(item.eventName);
                    }

                    return {
                        'element': element,
                        'event': {
                           'name': item.eventName,
                           'text': text
                        }
                    }
                }));
                break;
            default:
                console.error('Unknown action: ' + request.action);
        }
    });

})();
